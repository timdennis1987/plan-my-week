<?php

use App\Http\Controllers\MealPlannerController;
use App\Http\Controllers\MealController;
use Illuminate\Support\Facades\Route;

//Create Usernames
//ability to send recipes

//Route::get('welcome', [WelcomeController::class, 'index'])->name('index');

Auth::routes();

//Meal Planner
Route::get('/', [MealPlannerController::class, 'index'])->name('meal-planner');
Route::post('meal-planner/create',[MealPlannerController::class,'createMealPlan']);

Route::get('meal-search',[MealPlannerController::class,'mealSearchTable']);
Route::post('meal-search/results',[MealPlannerController::class,'mealSearchTableResults'])->name('meal.search');
Route::post('meal/selected',[MealPlannerController::class,'mealSelected']);

Route::get('meal-added',[MealPlannerController::class,'mealAdded']);
Route::get('meals/{id}/clear-week',[MealPlannerController::class,'clearWeek']);

Route::post('meals/edit-meal-plan/{id}',[MealPlannerController::class,'editMealPlan']);
Route::post('select-plan/{id}',[MealPlannerController::class,'selectMealPlan']);
Route::get('meal-plan/{id}/delete',[MealPlannerController::class,'deleteMealPlan']);

//Meals
Route::get('meals', [MealController::class, 'index'])->name('meals');
Route::post('meals/filter', [MealController::class, 'mealFilter']);
Route::get('meals/create', [MealController::class, 'create']);
Route::get('meals/clear', [MealController::class, 'clearFilter']);
Route::post('meals/store', [MealController::class, 'store']);
Route::get('meals/{id}/edit', [MealController::class, 'edit']);
Route::post('meals/{id}/update', [MealController::class, 'update']);
Route::get('meals/{id}/show', [MealController::class, 'show']);
Route::post('meal/{id}/user-rating', [MealController::class, 'userRating']);
Route::get('meal/{id}/delete', [MealController::class, 'deleteMeal']);
Route::post('meal/{id}/add', [MealController::class, 'addMealToUser']);
Route::post('meal/{id}/remove', [MealController::class, 'removeMealFromUser']);

Route::post('meals/save', [MealController::class, 'mealSaveToDateTime']);

