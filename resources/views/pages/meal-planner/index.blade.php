@extends('layouts.app')

@section('content')

    <style>

        a, .pointer {
            cursor: pointer;
        }

        h5 {
            margin-bottom: 0;
        }

        .card-body {
            background: #e9e9e9;
            padding: 0.1rem;
            min-height: 125px;
        }

        small, .small {
            font-size: 0.7em;
            margin-left: 4px;
        }

        .meal-type {
            position: absolute;
            top: 6px;
            left: 4px;
        }

        .meal-text {
            font-weight: bold;
            margin: 5px;
            width: 145px;
        }

        .meal-link {
            text-decoration: none;
            color: #000;
        }

        .meal-link:hover {
            color: #626060;
        }

        .meal-form-modal {
            min-height: 400px;
            overflow-y: auto;
        }

        .carousel-indicators {
            bottom: -45px;
        }

        .carousel-control-prev, .carousel-control-next {
            bottom: -433px;
        }
        .active-plan {
            background: #F0F8E6;
        }
        .hide {
            display: none;
        }
        .show {
            display: block;
        }
    </style>

    <div class="row justify-content-center">

        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif

        <div class="d-grid gap-2 d-md-flex justify-content-md-start">
            <h1>{{ $selectedPlan->title }}</h1>
        </div>

        <div class="d-grid gap-2 d-md-flex justify-content-md-end mb-3">
             <a class="btn btn-link" data-bs-toggle="modal" data-bs-target="#edit_meal_plan">Rename Plan</a>
        </div>

        <div class="d-grid gap-2 d-md-flex justify-content-md-end mb-5">
            <a href="/meals/{{ $selectedPlan->id }}/clear-week" class="btn btn-outline-danger">Clear Week</a>
            @if($selectedPlan->can_delete == 1)
                <a href="/meal-plan/{{ $selectedPlan->id }}/delete" class="btn btn-danger">Delete Meal Plan</a>
            @endif
        </div>

        <div class="d-block d-xxl-none">
            @include('includes.this-week-mobile-view')
        </div>

        <div class="d-none d-xxl-block">
            @include('includes.this-week-desktop-view')
        </div>

    </div>

    <div class="row mt-3">
        <div class="col">
            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#meal_plan">
                Create New Meal Plan
            </button>
        </div>
    </div>

    <!-- Creat New Plan -->
    <div class="modal fade" id="meal_plan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="/meal-planner/create" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add New Meal Plan</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <div class="mb-6">
                            <label
                                class="block mb-2 uppercase font-bold text-sm text-gray-700"
                                for="title"
                            >
                                Meal Plan Title
                            </label>

                            <input
                                class="border border-gray-400 p-2 w-full"
                                type="text"
                                name="title"
                                id="title"
                                required
                            >
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="success" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Existing Plan -->
    <div class="modal fade" id="edit_meal_plan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="/meals/edit-meal-plan/{{ $selectedPlan->id }}" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Meal Plan</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <div class="mb-6">
                            <label
                                class="block mb-2 uppercase font-bold text-sm text-gray-700"
                                for="title"
                            >
                                Meal Plan Title
                            </label>

                            <input
                                class="border border-gray-400 p-2 w-full"
                                type="text"
                                name="title"
                                id="title"
                                value="{{ $selectedPlan->title }}"
                                required
                            >
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="success" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row mt-3">

        <h4>Your Meal Plans</h4>

        @foreach($mealPlans as $plan)
        <div class="col-12 col-xs-6 col-md-4 col-xxl-2">
            <form id="form_{{ $plan->id }}" action="/select-plan/{{ $plan->id }}" method="POST">
                @csrf
                <div class="card shadow-sm pointer mb-2" onclick="$(this).closest('form').submit();">
                    <div class="card-body p-2 {{ $plan->id == \Auth::user()->meal_plan ? 'active-plan' : '' }}" style="min-height: 0;">
                        <h5>{{ $plan->title }}</h5>
                    </div>
                </div>
            </form>
        </div>
        @endforeach
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script>
        var myCarousel = document.querySelector('#myCarousel')
    </script>

    <script language="javascript" type="text/javascript">
        function submitDetailsForm() {
            $("#formId").submit();
        }
    </script>

    <script>
        $(document).ready(function() {

            $('.mobile-modal').click(function (){

                $("#mobile-iframe").hide();
                $(".fa-spin").show().delay(1575).fadeOut();
                setTimeout(
                    function() {
                        $("#mobile-iframe").show()
                    }, 2000);

                const day = $(this).attr('data-mobile-day');
                const type = $(this).attr('data-mobile-type');
                $.ajax({
                    url: 'meal-search',
                    type: 'GET',
                    data: {
                        "day": day,
                        "type": type
                    },
                    success:function () {
                        const iframeData = '?day='+day+'&type='+type;
                        $('#mobile-iframe').attr('src','{{ url('meal-search') }}' + iframeData);
                    }
                })
            })

            $('.desktop-modal').click(function (){

                $("#desktop-iframe").hide();
                $(".fa-spin").show().delay(1575).fadeOut();
                setTimeout(
                    function() {
                        $("#desktop-iframe").show()
                    }, 2000);

                const day = $(this).attr('data-desktop-day');
                const type = $(this).attr('data-desktop-type');
                $.ajax({
                    url: 'meal-search',
                    type: 'GET',
                    data: {
                        "day": day,
                        "type": type
                    },
                    success:function () {
                        const iframeData = '?day='+day+'&type='+type;
                        $('#desktop-iframe').attr('src','{{ url('meal-search') }}' + iframeData);
                    }
                })
            })
        });
    </script>
@endsection
