@extends('layouts.app')
@section('content')

    <h1>Meal Planner</h1>

    <div class="card shadow-sm text-center mt-5">
        <div class="card-body py-5">
            <form action="/meal-planner/create" method="POST">
                @csrf
                <h3 class="card-title">Hey {{ \Auth::user()->first_name }}</h3>
                <p class="card-text">Let's get you set up...</p>
                <button type="submit" href="meal-planner/create" class="btn btn-primary mt-3">Add a meal plan
                </button>
            </form>
        </div>
    </div>

@stop
