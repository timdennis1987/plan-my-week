@extends('layouts.app')
@section('content')

    <style>
        .pointer {
            cursor: pointer;
        }

        .meal-title-link {
            color: #0d6efd;
        }

        .meal-title-link:hover {
            opacity: .7;
        }

        img {
            height: 250px;
            object-fit: cover;
        }
    </style>

    <h1>Meals</h1>

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @elseif(session('error'))
        <div class="alert alert-danger" role="alert">
            {{ session('error') }}
        </div>
    @endif

    @include('includes.meal-search-filters')

    @if($meals)

        @include('includes.meal-search-grid')

    @else
        <div class="row">
            <div class="col">
                <h3 class="mt-5 text-danger text-center">No Meals!</h3>
            </div>
        </div>
    @endif

@stop
