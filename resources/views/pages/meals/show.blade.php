@extends('layouts.app')
@section('content')

    <div class="row">
        <h3 class="fw-bold">{{ $meal->title }}</h3>
    </div>
    @if($userAddedMeal)
        <div class="row">
            <h3><i class="fas fa-user me-3 pointer" data-bs-toggle="modal"
                   data-bs-target="#user-rating"></i>{!! $userRating->rating_stars !!}</h3>
        </div>
    @endif
    @if($meal->public == 1)
        <div class="row my-3">
            <small><i class="fas fa-users me-3"></i>{!! $globalRating->rating_stars !!}</small>
        </div>
    @endif

    <div class="row">
        <div class="col-sm-12 col-lg-5">
            <img class="mx-auto d-block mb-3 shadow-sm p-3 mb-5 bg-white rounded"
                 src="{{asset('images/').'/'. $meal->title . '/' . $meal->image_name }}"
                 width="100%">
        </div>
        <div class="col-sm-12 col-lg-7">

            @if($createdBy->created_by != \Auth::user()->id)
                @if(!$userAddedMeal)
                    <form action="/meal/{{ $meal->id }}/add" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-outline-dark mb-2">Add Meal</button>
                    </form>
                @else
                    <form action="/meal/{{ $meal->id }}/remove" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-outline-dark mb-2">Remove Meal</button>
                    </form>
                @endif
            @endif
            @if($meal->public == 1)
                <small>Created
                    by: {{ $createdBy->created_by == \Auth::user()->id ? 'You' : $createdBy->first_name }}</small>
            @else
                <small>Meal is set to private: <a href="/meals/{{ $meal->id }}/edit" class="text-danger">Edit</a> the
                    meal if you wish to allow other users to see it.</small>
            @endif

            @if($meal->description)
                <h4 class="py-3 mt-3">{!! $meal->description !!} </h4>
            @endif

            @if($meal->url)
                <a class="text-success" target="_blank" href="{{ $meal->url }}">View recipe from website</a>
            @endif
            @if($meal->method)
                <br>
                <br>
                <a href="#method">See Method</a>
            @endif
            <br>
            <br>

            @if($meal->vegan)
                <i class="fas fa-leaf"></i> <span class="me-3">Vegan</span>
            @endif
            @if($meal->slow_cook)
                <i class="fas fa-archive"></i> <span class="me-3">Slow Cooker</span>
            @endif
            @if($meal->budget)
                <i class="fas fa-pound-sign"></i> <span class="me-3">Budget</span>
            @endif

            <div class="card-group mt-3">
                <div class="card text-center">
                    <div class="card-body">
                        <h5 class="card-title"><i class="fas fa-user"></i></h5>
                        <p class="card-text">Serves {{ $meal->serving }}</p>
                    </div>
                </div>
                @if($meal->time != 0)
                    <div class="card text-center">
                        <div class="card-body">
                            <h5 class="card-title"><i class="fas fa-clock"></i></h5>
                            <p class="card-text">{{ $meal->time }}</p>
                        </div>
                    </div>
                @endif
                @if($meal->cal != 0)
                    <div class="card text-center">
                        <div class="card-body">
                            <h5 class="card-title"><i class="fas fa-balance-scale-left"></i></h5>
                            <p class="card-text">{{ $meal->cal }} calories / serving</p>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <hr class="d-none d-lg-block">
        @if($meal->ingredients)
            <div class="col-md-6">
                <h3 class="pt-3">Ingredients</h3>
                <p style="font-size: 8px;">{!! $meal->ingredients !!}</p>
            </div>
        @endif
        <hr class="d-block d-md-none">
        <div class="col-md-6">
            @if($meal->method)
                <h3 class="pt-3" id="method">Method</h3>
                <p>{!! $meal->method !!}</p>
            @endif
            @if($createdBy->created_by == \Auth::user()->id)
                <hr>
                <a href="/meals/{{ $meal->id }}/edit" class="text-danger">Edit Meal</a>
            @endif
        </div>
    </div>

    <!-- User rating modal -->
    <div class="modal fade" id="user-rating" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="/meal/{{ $meal->id }}/user-rating" method="POST">
                    @csrf

                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Rating</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <div class="col">
                            <label for="rating" class="form-label">Rating</label>
                            <select id="rating" class="form-select" name="rating">
                                @foreach($ratings as $rating)
                                    <option value="{{ $rating->rating }}">{{ $rating->rating_name }}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save Rating</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop
