@extends('layouts.search-template')
@section('content')

    <div class="row">
        <div class="d-grid gap-2 d-md-flex text-center mt-5">
            <button class="btn btn-success btn-lg" onclick="javascript:parent.location.reload()">Click to close/refresh...</button>
        </div>
    </div>

@stop
