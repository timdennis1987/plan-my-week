@extends('layouts.app')
@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/meals">Meals</a></li>
            <li class="breadcrumb-item active" aria-current="page">Create</li>
            <li class="breadcrumb-item"><a href="/meals/{{ $meal->id }}/show">Preview</a></li>
        </ol>
    </nav>

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif

    <form class="row g-3" action="/meals/{{ $meal->id }}/update" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="col-md-6">
            <label for="title" class="form-label">Title</label>
            <input type="text" class="form-control" id="title" name="title" value="{{ $meal->title }}">
        </div>
        <div class="col-md-3">
            <label for="serving" class="form-label">Serves</label>
            <select id="serving" class="form-select" name="serving">
                <option value="1" {{ $meal->serving == 1 ? 'selected' : '' }}>1</option>
                <option value="2" {{ $meal->serving == 2 ? 'selected' : '' }}>2</option>
                <option value="4" {{ $meal->serving == 4 ? 'selected' : '' }}>4</option>
                <option value="6" {{ $meal->serving == 6 ? 'selected' : '' }}>6</option>
                <option value="8" {{ $meal->serving == 8 ? 'selected' : '' }}>8</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="cal" class="form-label">Calories</label>
            <input type="text" class="form-control" id="cal" placeholder="123..." name="cal" value="{{ $meal->cal }}">
        </div>
        <div class="col-md-3">
            <label for="vegan" class="form-label">Vegan</label>
            <select id="vegan" class="form-select" name="vegan">
                <option value="0" {{ $meal->vegan == 0 ? 'selected' : '' }}>No</option>
                <option value="1" {{ $meal->vegan == 1 ? 'selected' : '' }}>Yes</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="time" class="form-label">Time</label>
            <input type="text" class="form-control" id="time" placeholder="30..." name="time" value="{{ $meal->time }}">
        </div>
        <div class="col-md-3">
            <label for="slow_cook" class="form-label">Slow Cooker</label>
            <select id="slow_cook" class="form-select" name="slow_cook">
                <option value="0" {{ $meal->slow_cook == 0 ? 'selected' : '' }}>No</option>
                <option value="1" {{ $meal->slow_cook == 1 ? 'selected' : '' }}>Yes</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="budget" class="form-label">Budget</label>
            <select id="budget" class="form-select" name="budget">
                <option value="0" {{ $meal->budget == 0 ? 'selected' : '' }}>No</option>
                <option value="1" {{ $meal->budget == 1 ? 'selected' : '' }}>Yes</option>
            </select>
        </div>
        <div class="col-md-12">
            <label for="description" class="form-label">Description</label>
            <textarea class="form-control" id="description" rows="10" name="description">{!! $meal->description !!}</textarea>
        </div>
        <div class="col-md-12">
            <label for="ingredients" class="form-label">Ingredients</label>
            <textarea class="form-control" id="ingredients" rows="10" name="ingredients">{!! $meal->ingredients !!}</textarea>
        </div>
        <div class="col-md-12">
            <label for="method" class="form-label">Method</label>
            <textarea class="form-control" id="method" rows="10" name="meal_method">{!! $meal->method !!}</textarea>
        </div>
        <div class="col-md-6">
            <label for="url" class="form-label">URL</label>
            <input type="text" class="form-control" id="url" name="url" value="{{ $meal->url }}">
        </div>
        <div class="col-md-6">
            <label for="rating" class="form-label">Rating</label>
            <select id="rating" class="form-select" name="rating">
                @foreach($ratings as $rating)
                    <option value="{{ $rating->rating }}" {{ $meal->rating == $rating->rating ? 'selected' : '' }}>
                        {{ $rating->rating_name }}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="col-md-6">
            <label for="url" class="form-label">Image</label>
            <input type="text" class="form-control" id="url" value="{{ $meal->image_name }}" disabled>
        </div>
        <div class="mb-3">
            <label for="image" class="form-label">Image</label>
            <input type="file" class="form-about_image-file" id="image" name="image">
        </div>
        <div class="col">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="1" id="public" name="public" {{ $meal->public == 1 ? 'checked' : '' }}>
                <label class="form-check-label" for="public">
                    Allow others users to see this meal?
                </label>
            </div>
        </div>
        <div class="col-12 pt-3">
            <button type="submit" class="btn btn-success">Update Meal</button>
            <a href="/meal/{{ $meal->id }}/delete" class="btn btn-outline-danger ms-2">Delete Meal</a>
        </div>
    </form>

@stop
