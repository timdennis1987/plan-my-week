<form action="/meals/filter" method="POST" class="row row-cols-lg-auto g-3 align-items-center">
    @csrf
    <div class="col-12">
        <div class="input-group">
            <input type="text"
                   class="form-control"
                   name="search"
                   placeholder="Search..."
                   value="{{ isset($_GET['search']) ? $_GET['search'] : '' }}">
        </div>
    </div>

    <div class="col-6">
        <select class="form-select" name="rating">
            <option selected>- Rating -</option>
            @foreach($ratings as $rating)
                <option
                    value="{{ $rating->rating }}"
                    {{ isset($_GET['rating']) && $_GET['rating'] == $rating->rating ? 'selected' : '' }}>
                    {{ $rating->rating_name }}
                </option>
            @endforeach
        </select>
    </div>

    <div class="col-6">
        <select class="form-select" name="vegan">
            <option selected>- Vegan -</option>
            <option value="0" {{ isset($_GET['vegan']) && $_GET['vegan'] == 0 ? 'selected' : '' }}>No</option>
            <option value="1" {{ isset($_GET['vegan']) && $_GET['vegan'] == 1 ? 'selected' : '' }}>Yes</option>
        </select>
    </div>

    <div class="col-6">
        <select class="form-select" name="slow_cook">
            <option selected>- Slow Cooker -</option>
            <option value="0" {{ isset($_GET['slow_cook']) && $_GET['slow_cook'] == 0 ? 'selected' : '' }}>No
            </option>
            <option value="1" {{ isset($_GET['slow_cook']) && $_GET['slow_cook'] == 1 ? 'selected' : '' }}>Yes
            </option>
        </select>
    </div>

    <div class="col-6">
        <select class="form-select" name="budget">
            <option selected>- Budget -</option>
            <option value="0" {{ isset($_GET['budget']) && $_GET['budget'] == 0 ? 'selected' : '' }}>No</option>
            <option value="1" {{ isset($_GET['budget']) && $_GET['budget'] == 1 ? 'selected' : '' }}>Yes</option>
        </select>
    </div>

    <div class="col-12">
        <button type="submit" class="btn btn-primary">Search</button>
        <a href="/meals/clear" class="btn btn-outline-danger ms-2">Clear Filter</a>
        <a href="/meals/create" class="btn btn-outline-success ms-2">Create Meal</a>
    </div>
</form>
