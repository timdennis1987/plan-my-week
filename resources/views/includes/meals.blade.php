@extends('layouts.app')
@section('content')

    <style>
        .pointer {
            cursor: pointer;
        }

        .meal-title-link {
            color: #0d6efd;
        }

        .meal-title-link:hover {
            opacity: .7;
        }

        img {
            height: 250px;
            object-fit: cover;
        }
    </style>

    <h1>Meals</h1>

    <form action="/meals/filter" method="POST" class="row row-cols-lg-auto g-3 align-items-center">
        @csrf
        <div class="col-12">
            <div class="input-group">
                <input type="text"
                       class="form-control"
                       name="search"
                       placeholder="Search..."
                       value="{{ isset($_GET['search']) ? $_GET['search'] : '' }}">
            </div>
        </div>

        <div class="col-6">
            <select class="form-select" name="rating">
                <option selected>- Rating -</option>
                @foreach($ratings as $rating)
                    <option
                        value="{{ $rating->rating }}"
                        {{ isset($_GET['rating']) && $_GET['rating'] == $rating->rating ? 'selected' : '' }}>
                        {{ $rating->rating_name }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="col-6">
            <select class="form-select" name="vegan">
                <option selected>- Vegan -</option>
                <option value="0" {{ isset($_GET['vegan']) && $_GET['vegan'] == 0 ? 'selected' : '' }}>No</option>
                <option value="1" {{ isset($_GET['vegan']) && $_GET['vegan'] == 1 ? 'selected' : '' }}>Yes</option>
            </select>
        </div>

        <div class="col-6">
            <select class="form-select" name="slow_cook">
                <option selected>- Slow Cooker -</option>
                <option value="0" {{ isset($_GET['slow_cook']) && $_GET['slow_cook'] == 0 ? 'selected' : '' }}>No
                </option>
                <option value="1" {{ isset($_GET['slow_cook']) && $_GET['slow_cook'] == 1 ? 'selected' : '' }}>Yes
                </option>
            </select>
        </div>

        <div class="col-6">
            <select class="form-select" name="budget">
                <option selected>- Budget -</option>
                <option value="0" {{ isset($_GET['budget']) && $_GET['budget'] == 0 ? 'selected' : '' }}>No</option>
                <option value="1" {{ isset($_GET['budget']) && $_GET['budget'] == 1 ? 'selected' : '' }}>Yes</option>
            </select>
        </div>

        <div class="col-12">
            <button type="submit" class="btn btn-primary">Search</button>
            <a href="/meals/clear" class="btn btn-outline-danger ms-2">Clear Filter</a>
            <a href="/meals/create" class="btn btn-outline-success ms-2">Create Meal</a>
        </div>
    </form>

    @if($meals)

        <div class="row row-cols-1 row-cols-xs-1 row-cols-sm-2 row-cols-lg-4 g-4 mt-5">
            @foreach($meals as $meal)
                <div class="col">
                    <div class="card shadow">
                        <a href="/meals/{{ $meal->id }}/show">
                            <img class="card-img-top"
                                 src="{{asset('images/').'/'. $meal->title . '/' . $meal->image_name }}"
                                 width="100%">
                        </a>
                        <div class="card-body">
                            <a href="/meals/{{ $meal->id }}/show">
                                <h5 class="card-title">{{ strlen($meal->title) > 25 ? substr($meal->title, 0, 25) . '...' : $meal->title }}</h5>
                            </a>
                            <p class="card-text">{!! $meal->rating_stars !!}</p>
                            <button class="btn btn-outline-success" data-bs-toggle="modal"
                                    data-bs-target="#meal_{{ $meal->id }}">Add
                            </button>
                            <a class="btn btn-outline-warning" href="/meals/{{ $meal->id }}/edit">Edit</a>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">

                                <i class="fas fa-balance-scale-left"></i> <span class="me-3">{{ $meal->cal }} cal</span>

                                @if($meal->vegan)
                                    <i class="fas fa-leaf"></i> <span class="me-3">Vegan</span>
                                @endif
                                @if($meal->slow_cook)
                                    <i class="fas fa-archive"></i> <span class="me-3">Slow Cooker</span>
                                @endif
                                @if($meal->budget)
                                    <i class="fas fa-pound-sign"></i> <span class="me-3">Budget</span>
                                @endif
                            </small>
                        </div>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="meal_{{ $meal->id }}" tabindex="-1" aria-labelledby="mealModal"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <form action="/meals/save" method="POST">
                                @csrf

                                <div class="modal-header">
                                    <h5 class="modal-title" id="mealModal">{{ $meal->title }}</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                </div>
                                <div class="modal-body">

                                    <div class="row">
                                        <div class="col-6">
                                            <select class="form-select" name="vegan">
                                                <option selected>- Day -</option>
                                                <option value="1">Monday</option>
                                                <option value="2">Tuesday</option>
                                                <option value="3">Wednesday</option>
                                                <option value="4">Thursday</option>
                                                <option value="5">Friday</option>
                                                <option value="6">Saturday</option>
                                                <option value="7">Sunday</option>
                                            </select>
                                        </div>

                                        <div class="col-6">
                                            <select class="form-select" name="vegan">
                                                <option selected>- Meal -</option>
                                                <option value="1">Breakfast</option>
                                                <option value="2">Lunch</option>
                                                <option value="3">Dinner</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close
                                    </button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    @else
        <div class="row">
            <div class="col">
                <h3 class="mt-5 text-danger text-center">No Meals!</h3>
            </div>
        </div>
    @endif

@stop
