<div class="row row-cols-1 row-cols-xs-1 row-cols-sm-2 row-cols-lg-4 g-4 mt-5">
    @foreach($meals as $meal)
        <div class="col">
            <div class="card shadow">
                <a href="/meals/{{ $meal->id }}/show">
                    <img class="card-img-top"
                         src="{{asset('images/').'/'. $meal->title . '/' . $meal->image_name }}"
                         width="100%">
                </a>
                <div class="card-body">
                    <a href="/meals/{{ $meal->id }}/show">
                        <h5 class="card-title">{{ strlen($meal->title) > 25 ? substr($meal->title, 0, 25) . '...' : $meal->title }}</h5>
                    </a>

                    <p class="card-text"><i class="fas fa-user me-2"></i>{!! $meal->rating_stars !!}</p>

                    <button class="btn btn-outline-success" data-bs-toggle="modal"
                            data-bs-target="#meal_{{ $meal->id }}">Add
                    </button>
                    @if($meal->created_by == \Auth::user()->id)
                        <a class="btn btn-outline-warning" href="/meals/{{ $meal->id }}/edit">Edit</a>
                    @endif
                    <br>
                    <div class="row pt-2">
                        @if($meal->public)
                            <small class="text-success"><i class="fas fa-users me-2"></i>Meal is public</small>
                        @else
                            <small class="text-danger"><i class="fas fa-users-slash me-2"></i>Meal is private</small>
                        @endif
                    </div>
                </div>
                <div class="card-footer">
                    <small class="text-muted">

                        <i class="fas fa-balance-scale-left"></i> <span class="me-3">{{ $meal->cal }} cal</span>

                        @if($meal->vegan)
                            <i class="fas fa-leaf"></i> <span class="me-3">Vegan</span>
                        @endif
                        @if($meal->slow_cook)
                            <i class="fas fa-archive"></i> <span class="me-3">Slow Cooker</span>
                        @endif
                        @if($meal->budget)
                            <i class="fas fa-pound-sign"></i> <span class="me-3">Budget</span>
                        @endif
                    </small>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="meal_{{ $meal->id }}" tabindex="-1" aria-labelledby="mealModal"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <form action="/meal/selected" method="POST">
                        @csrf

                        <input type="hidden" value="{{ $meal->id }}" name="meal">
                        <input type="hidden" value="1" name="submitted_from_meal_page">
                        <div class="modal-header">
                            <h5 class="modal-title" id="mealModal">{{ $meal->title }}</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                        </div>
                        <div class="modal-body">

                            <div class="row">
                                <div class="col-6">
                                    <select class="form-select" name="day">
                                        <option selected>- Day -</option>
                                        @foreach($days as $day)
                                            <option value="{{ $day->id }}">{{ $day->day }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-6">
                                    <select class="form-select" name="meal_type">
                                        <option selected>- Meal -</option>
                                        @foreach($mealTypes as $mealType)
                                            <option value="{{ $mealType->id }}">{{ $mealType->type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close
                            </button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endforeach
</div>
