<div id="carouselExampleCaptions" class="carousel carousel-dark slide mb-5" data-bs-ride="carousel">
    <div class="carousel-indicators">
        @php $pos=1 @endphp
        @foreach($daysInWeek as $day)
            <button type="button"
                    data-bs-target="#carouselExampleCaptions"
                    data-bs-slide-to="{{ $pos -1 }}"
                    class="{{ Carbon\Carbon::now()->format('l') == $day->day ? 'active' : '' }}"
                    aria-current="true"
                    aria-label="Slide {{ $pos }}"></button>
            @php $pos++ @endphp
        @endforeach
    </div>

    <div class="carousel-inner">
        @php $pos=1 @endphp
        @foreach($daysInWeek as $day)
            <div class="carousel-item {{ Carbon\Carbon::now()->format('l') == $day->day ? 'active' : '' }}">
                <div class="row">
                    <div class="col">
                        <small class="{{ Carbon\Carbon::now()->format('l') == $day->day ? 'text-success fw-bold' : '' }}">Day {{ $pos }}</small>
                        <h5 class="{{ Carbon\Carbon::now()->format('l') == $day->day ? 'text-success fw-bold' : '' }}">{{ $day->day }}</h5>
                    </div>
                </div>

                @php $total = 0 @endphp
                @foreach($mealTypes as $type)

                    <div class="card shadow-sm d-flex mt-3">
                        <div class="card-body align-items-center d-flex justify-content-center">
                            <small class="meal-type">{{ $type->type }}</small>
                            <div class="text-center meal-text">
                                @foreach($meals as $meal)
                                    @if($meal->day == $pos && $meal->type == $type->id)
                                        @php $total = $total + $meal->cal @endphp
                                        @if($meal->id)
                                            <a href="/meals/{{ $meal->id }}/show" target="_blank">
                                                {{ strlen($meal->title) > 25 ? substr($meal->title, 0, 25) . '...' : $meal->title }}
                                            </a>
                                            <br>
                                            <a data-bs-toggle="modal"
                                               data-bs-target="#day_mobile"
                                               data-mobile-day="{{ $day->id }}"
                                               data-mobile-type="{{ $type->id }}"
                                               class="meal-link mobile-modal">
                                                <i class="fas fa-edit text-success"></i>
                                            </a>
                                        @else
                                            <a data-bs-toggle="modal"
                                               data-bs-target="#day_mobile"
                                               data-mobile-day="{{ $day->id }}"
                                               data-mobile-type="{{ $type->id }}"
                                               class="meal-link mobile-modal">
                                                <i class="fas fa-plus text-success"></i>
                                            </a>
                                        @endif
                                    @endif

                                @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach
                <h5 class="my-3">Total Cal: {{ $total }}</h5>
            </div>
            @php $pos++ @endphp
        @endforeach
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="day_mobile" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
            </div>
            <div class="modal-body meal-form-modal text-center">
                <i class="fas fa-circle-notch fa-spin fa-5x mt-5 text-info"></i>
                <iframe id="mobile-iframe"
                        src=""
                        style="display: none;overflow:hidden;height:370px;width:100%"></iframe>
            </div>
        </div>
    </div>
</div>
