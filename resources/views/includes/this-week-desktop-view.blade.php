<div class="row">

    <div id="click-me">test</div>

    @php $pos = 1 @endphp
    @foreach($daysInWeek as $day)

        <div class="col">
            <small
                class="{{ Carbon\Carbon::now()->format('l') == $day->day ? 'text-success fw-bold' : '' }}">Day {{ $pos }}</small>
            <h5 class="{{ Carbon\Carbon::now()->format('l') == $day->day ? 'text-success fw-bold' : '' }}">{{ $day->day }}</h5>

            @php $total = 0 @endphp
            @foreach($mealTypes as $type)

                <div class="card shadow-sm d-flex mt-3">
                    <div class="card-body align-items-center d-flex justify-content-center">
                        <small class="meal-type">{{ $type->type }}</small>
                        <div class="text-center meal-text">
                            @foreach($meals as $meal)
                                @if($meal->day == $pos && $meal->type == $type->id)
                                    @php $total = $total + $meal->cal @endphp
                                    @if($meal->id)
                                        @if($meal->public || $meal->created_by == \Auth::user()->id)
                                            <a href="/meals/{{ $meal->id }}/show" target="_blank">
                                                {{ strlen($meal->title) > 25 ? substr($meal->title, 0, 25) . '...' : $meal->title }}
                                            </a>
                                        @else
                                            <span
                                                class="text-secondary">{{ strlen($meal->title) > 25 ? substr($meal->title, 0, 25) . '...' : $meal->title }}</span>
                                        @endif
                                        <br>
                                        <a data-bs-toggle="modal"
                                           data-bs-target="#day_desktop"
                                           data-desktop-day="{{ $day->id }}"
                                           data-desktop-type="{{ $type->id }}"
                                           class="meal-link desktop-modal">
                                            <i class="fas fa-edit text-success"></i>
                                        </a>
                                    @else
                                        <a data-bs-toggle="modal"
                                           data-bs-target="#desktop_modal"
                                           data-desktop-day="{{ $day->id }}"
                                           data-desktop-type="{{ $type->id }}"
                                           class="meal-link desktop-modal">
                                            <i class="fas fa-plus text-success"></i>
                                        </a>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>

            @endforeach
            <h6 class="my-3">Total Cal: {{ $total }}</h6>
            @php $pos++ @endphp
        </div>
    @endforeach

</div>

<!-- Modal -->
<div class="modal fade" id="desktop_modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
            </div>
            <div class="modal-body meal-form-modal text-center">
                <i class="fas fa-circle-notch fa-spin fa-5x mt-5 text-info"></i>

                <iframe id="desktop-iframe"
                        src=""
                        style="display: none; overflow:hidden;height:370px;width:100%"></iframe>
            </div>
        </div>
    </div>
</div>
