@extends('layouts.search-template')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="" method="POST">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" placeholder="Search meal..." id="search">
                                </div>
                            </div>
                        </div>
                    </form>
                    <form action="/meal/selected" method="POST">
                        @csrf

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <select class="form-control" name="day">
                                        <option selected>- Day -</option>
                                        @foreach($days as $day)
                                            <option
                                                value="{{ $day->id }}" {{ isset($_GET['day']) && $_GET['day'] == $day->id ? 'selected' : '' }}>
                                                {{ $day->day }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">
                                    <select class="form-control" name="meal_type">
                                        <option selected>- Meal -</option>
                                        @foreach($mealTypes as $mealType)
                                            <option
                                                value="{{ $mealType->id }}" {{ isset($_GET['type']) && $_GET['type'] == $mealType->id ? 'selected' : '' }}>
                                                {{ $mealType->type }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <button class="btn btn-outline-danger" type="submit">Clear</button>
                            </div>
                        </div>

                        <table class="table table-striped table-inverse table-responsive d-table">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Meal</th>
                                <th>Calories</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <button type="submit" class="btn btn-success">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
