<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Meal Search</title>
</head>
<body>

<main>
    <div class="container">
        @yield('content')
    </div>
</main>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
{{-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

<script>
    $('#search').on('keyup', function () {
        search();
    });
    search();

    function search() {
        var keyword = $('#search').val();
        $.post('{{ route("meal.search") }}',
            {
                _token: $('meta[name="csrf-token"]').attr('content'),
                keyword: keyword
            },
            function (data) {
                table_post_row(data);
                console.log(data);
            });
    }

    // table row with ajax
    function table_post_row(res) {
        let htmlView = '';
        if (res.meals.length <= 0) {
            htmlView += `
       <tr>
          <td colspan="4">Search for a meal.</td>
      </tr>`;
        }
        for (let i = 0; i < res.meals.length; i++) {
            let meal = res.meals[i].id;
            htmlView += `
        <tr>
            <td><input type="radio" name="meal" value="`+meal+`"></td>
            <td>` + res.meals[i].title + `</button></td>
            <td>` + res.meals[i].cal + `</td>
        </tr>`;
        }
        $('tbody').html(htmlView);
    }
</script>
</body>
</html>
