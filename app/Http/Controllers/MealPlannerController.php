<?php

namespace App\Http\Controllers;

use App\Models\Meal;
use App\Models\MealPlan;
use App\Models\MealPlanData;
use App\Models\User;
use App\Models\UserMeal;
use Illuminate\Http\Request;

class MealPlannerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (\Auth::user()->meal_plan == 0) {
            return view('pages.meal-planner.new-user');
        }

        $selectedMealPlan = User::join('meal_plans', 'meal_plans.id', '=', 'users.meal_plan')
            ->where('users.id', \Auth::user()->id)
            ->first(['meal_plans.*']);

        $daysInWeek = \DB::table('days')->get();
        $mealTypes  = \DB::table('meal_types')->get();

        $meals = User::where('users.id', \Auth::user()->id)
            ->join('meal_plan_data', 'meal_plan_data.meal_plan_id', '=', 'users.meal_plan')
            ->leftJoin('meals', 'meals.id', '=', 'meal_plan_data.meal_id')
            ->get([
                'meals.id',
                'meals.title',
                'meals.cal',
                'meals.public',
                'meals.created_by',
                'meal_plan_data.day',
                'meal_plan_data.type',
                'meal_plan_data.id as meal_plan_data_id'
            ]);

        $mealPlans = MealPlan::where('user_id', \Auth::user()->id)->get();

        return view('pages.meal-planner.index', [
            'selectedPlan' => $selectedMealPlan,
            'meals'        => $meals,
            'daysInWeek'   => $daysInWeek,
            'mealTypes'    => $mealTypes,
            'mealPlans'    => $mealPlans
        ]);
    }

    public function mealSearchTable()
    {
        $days      = \DB::table('days')->get();
        $mealTypes = \DB::table('meal_types')->get();

        return view('includes.meal-search-table', [
            'days'      => $days,
            'mealTypes' => $mealTypes,
        ]);
    }

    public function mealSearchTableResults(Request $request)
    {
//        dd(1);
//        $meals = Meal::join('user_meals', 'user_meals.meal_id', '=', 'meals.id')
//            ->where('user_meals.user_id', \Auth::user()->id)
//            ->get([
//                'meals.id',
//                'meals.title'
//            ]);

        $meals = UserMeal::where('user_meals.user_id', \Auth::user()->id)
            ->join('meals', 'meals.id', '=', 'user_meals.meal_id')
            ->get([
                'meals.id',
                'meals.title'
            ]);

        if ($request->keyword != '') {
            $meals = Meal::where('title', 'LIKE', '%' . $request->keyword . '%')->get();
        }
        return response()->json([
            'meals' => $meals
        ]);
    }

    public function mealSelected(Request $request)
    {
        $usersMealPan = \Auth::user()->meal_plan;

        $thisWeek = MealPlanData::where('meal_plan_id', $usersMealPan)
            ->where('day', $request->day)
            ->where('type', $request->meal_type)
            ->first();

        if ($request->meal) {
            $thisWeek->update([
                'meal_id' => $request->meal
            ]);
        } else {
            $thisWeek->update([
                'meal_id' => $request->meal
            ]);
        }

        if ($request->submitted_from_meal_page) {
            return redirect()->back()->with('success', 'Meal added!');
        }
        return redirect('/meal-added')->with('success', 'Meal added!');
    }

    public function mealAdded()
    {
        return view('pages.meals.meal-added');
    }

    public function clearWeek($id)
    {
        $thisWeek = MealPlanData::where('meal_plan_id', $id)->get();

        foreach ($thisWeek as $day) {
            $day->update([
                'meal_id' => NULL
            ]);
        }

        return redirect()->back()->with('success', 'Week cleared!');
    }

    public function createMealPlan(Request $request)
    {
        if (\Auth::user()->meal_plan == 0) {
            MealPlan::create([
                'title'      => 'This Week',
                'user_id'    => \Auth::user()->id,
                'can_delete' => 0
            ]);

        } else {
            MealPlan::create([
                'title'      => $request->title,
                'user_id'    => \Auth::user()->id,
                'can_delete' => 1
            ]);
        }

        $mealPlanId = MealPlan::where('user_id', \Auth::user()->id)->orderBy('id', 'desc')->first()->id;

        for ($day = 1; $day <= 7; $day++) {
            for ($mealType = 1; $mealType <= 3; $mealType++) {
                MealPlanData::create([
                    'meal_plan_id' => $mealPlanId,
                    'day'          => $day,
                    'meal_id'      => NULL,
                    'type'         => $mealType
                ]);
            }
        }

        $mealPlan = \Auth::user();

        $mealPlan->update([
            'meal_plan' => $mealPlanId
        ]);

        return redirect()->back();
    }

    public function editMealPlan(Request $request, $id)
    {

        $mealPlan = MealPlan::where('id', $id)->first();

        $mealPlan->update([
            'title' => $request->title
        ]);

        return redirect()->back();
    }

    public function selectMealPlan(Request $request, $id)
    {
        $user = \Auth::user();

        $user->update([
            'meal_plan' => $id
        ]);

        return redirect()->back();
    }

    public function deleteMealPlan($id)
    {
        $mealPlanToDelete = MealPlan::find($id);
        $firstMealPlan    = MealPlan::where('user_id', \Auth::user()->id)->orderBy('id')->first();

        \Auth::user()->update(['meal_plan' => $firstMealPlan->id]);

        $mealPlanMeals = MealPlanData::where('meal_plan_id', $mealPlanToDelete->id)->get();

        foreach ($mealPlanMeals as $meal) {
            $meal->delete();
        }

        $mealPlanToDelete->delete();

        return redirect()->back()->with('success', 'Meal plan deleted');
    }

    public function test($id)
    {
        dd($id);
    }
}
