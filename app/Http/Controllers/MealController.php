<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Meal;
use App\Models\Rating;
use App\Models\MealPlanData;
use App\Models\UserMeal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use function PHPUnit\Framework\isEmpty;

class MealController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $ratings   = UserMeal::get();
        $meals     = $this->getMeals();
        $days      = \DB::table('days')->get();
        $mealTypes = \DB::table('meal_types')->get();

        return view('pages.meals.index', [
            'ratings'   => $ratings,
            'meals'     => $meals,
            'days'      => $days,
            'mealTypes' => $mealTypes
        ]);
    }

    public function getMeals()
    {
        $userMeals = UserMeal::where('user_id', \Auth::user()->id)->get();

        if (count($userMeals) > 0) {
            if (isset($_GET['search'])) {

                $meals = Meal::join('images', 'images.meal_id', '=', 'meals.id')
                    ->join('user_meals', 'user_meals.meal_id', '=', 'meals.id')
                    ->join('ratings', 'ratings.rating', '=', 'user_meals.rating')
                    ->where('user_meals.user_id', \Auth::user()->id)
                    ->when($_GET['search'] != '', function ($query) {
                        return $query->where('title', 'LIKE', '%' . $_GET['search'] . '%');
                    })
                    ->when($_GET['rating'] != '- Rating -', function ($query) {
                        return $query->where('meals.rating', $_GET['rating']);
                    })
                    ->when($_GET['vegan'] != '- Vegan -', function ($query) {
                        return $query->where('vegan', $_GET['vegan']);
                    })
                    ->when($_GET['slow_cook'] != '- Slow Cooker -', function ($query) {
                        return $query->where('slow_cook', $_GET['slow_cook']);
                    })
                    ->when($_GET['budget'] != '- Budget -', function ($query) {
                        return $query->where('budget', $_GET['budget']);
                    })
                    ->orderBy('title', 'asc')
                    ->get([
                        'meals.id',
                        'meals.title',
                        'meals.vegan',
                        'meals.slow_cook',
                        'meals.cal',
                        'meals.budget',
                        'meals.created_by',
                        'meals.public',
                        'ratings.rating_stars',
                        'images.name as image_name',
                        'images.path',
                    ]);

                if (count($meals) > 0) {
                    return $meals;
                } else {
                    return false;
                }

            } else {
                return Meal::join('images', 'images.meal_id', '=', 'meals.id')
                    ->join('user_meals', 'user_meals.meal_id', '=', 'meals.id')
                    ->join('ratings', 'ratings.rating', '=', 'user_meals.rating')
                    ->where('user_meals.user_id', \Auth::user()->id)
                    ->orderBy('title', 'asc')
                    ->get([
                        'meals.id',
                        'meals.title',
                        'meals.vegan',
                        'meals.slow_cook',
                        'meals.cal',
                        'meals.budget',
                        'meals.created_by',
                        'meals.public',
                        'ratings.rating_stars',
                        'images.name as image_name',
                        'images.path',
                    ]);
            }
        } else {
            return Null;
        }
    }

    public function mealFilter(Request $request)
    {
        if ($request->search ||
            $request->rating != '- Rating -' ||
            $request->slow_cook != '- Slow Cooker -' ||
            $request->budget != '- Budget -' ||
            $request->vegan != '- Vegan -') {
            return redirect('meals?search=' .
                $request->search .
                '&rating=' .
                $request->rating .
                '&slow_cook=' .
                $request->slow_cook .
                '&budget=' .
                $request->budget .
                '&vegan=' .
                $request->vegan);
        } else {
            return redirect('/meals');
        }
    }

    public function mealSaveToDateTime(Request $request)
    {
        return redirect()->back();
    }

    public function clearFilter()
    {
        return redirect('/meals');
    }

    public function create()
    {
        return view('pages.meals.create', [
            'ratings' => Rating::get()
        ]);
    }

    public function store(Request $request)
    {
        $mealTitle = $request->title;

        Meal::create([
            'title'       => $mealTitle,
            'description' => $request->description,
            'ingredients' => $request->ingredients,
            'method'      => $request->meal_method,
            'serving'     => $request->serving,
            'cal'         => $request->cal,
            'vegan'       => $request->vegan,
            'time'        => $request->time,
            'slow_cook'   => $request->slow_cook,
            'budget'      => $request->budget,
            'url'         => $request->url,
            'rating'      => $request->rating,
            'created_by'  => \Auth::user()->id,
            'public'      => $request->public == 1 ? 1 : 0,
            'created_at'  => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'  => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        $mealId = Meal::orderBy('id', 'desc')->first()->id;

        UserMeal::create([
            'meal_id' => $mealId,
            'user_id' => \Auth::user()->id
        ]);

        if ($request->file('image')) {
            $image           = $request->file('image');
            $filename        = $image->getClientOriginalName();
            $destinationPath = 'images/' . $mealTitle;
            $image->storeAs("$destinationPath", $filename);

            Image::create([
                'meal_id'    => $mealId,
                'path'       => $destinationPath . '/' . $filename,
                'name'       => $filename,
                'created_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
        }

        return redirect()->back()->with('success', 'Created meal successfully!');
    }

    public function show($mealId)
    {
        $selectedMeal = Meal::where('id', $mealId)->first();

        if ($selectedMeal->created_by != \Auth::user()->id) {
            if ($selectedMeal->public != 1) {

                UserMeal::where('user_id', \Auth::user()->id)->where('meal_id', $mealId)->first()->delete();

                return redirect('/meals')->with('error', 'Sorry, looks like the creator has made this meal private');
            }
        }

        $globalRating = Rating::where('rating', 0)->first();

        if (UserMeal::where('meal_id', $mealId)->first()) {
            $meal = Meal::join('images', 'images.meal_id', '=', 'meals.id')
                ->join('user_meals', 'user_meals.meal_id', '=', 'meals.id')
                ->join('ratings', 'ratings.rating', '=', 'user_meals.rating')
                ->where('meals.id', $mealId)
                ->get([
                    'meals.id',
                    'meals.title',
                    'meals.description',
                    'meals.ingredients',
                    'meals.method',
                    'meals.serving',
                    'meals.cal',
                    'meals.vegan',
                    'meals.time',
                    'meals.slow_cook',
                    'meals.budget',
                    'meals.url',
                    'meals.public',
                    'images.name as image_name',
                    'images.path',
                    'ratings.rating_stars'
                ]);

            $ratings = UserMeal::where('meal_id', $mealId)->get();

            $ratingValues = [];

            foreach ($ratings as $aRating) {
                $ratingValues[] = $aRating->rating;
            }

            $ratingAverage = collect($ratingValues)->sum() / $ratings->count();
            $globalRating  = Rating::where('rating', $ratingAverage)->first();
        } else {

            $meal = Meal::join('images', 'images.meal_id', '=', 'meals.id')
                ->where('meals.id', $mealId)
                ->get([
                    'meals.id',
                    'meals.title',
                    'meals.description',
                    'meals.ingredients',
                    'meals.method',
                    'meals.serving',
                    'meals.cal',
                    'meals.vegan',
                    'meals.time',
                    'meals.slow_cook',
                    'meals.budget',
                    'meals.url',
                    'meals.public',
                    'images.name as image_name',
                    'images.path',
                ]);
        }

        $userRating = UserMeal::join('ratings', 'ratings.rating', '=', 'user_meals.rating')
            ->where('user_id', \Auth::user()->id)
            ->where('meal_id', $mealId)
            ->first();

        $userAddedMeal = UserMeal::where('meal_id', $mealId)->where('user_id', \Auth::user()->id)->first();
        $createdBy     = Meal::join('users', 'users.id', '=', 'meals.created_by')->where('meals.id', $mealId)->first();

        return view('pages.meals.show', [
            'meal'          => $meal[0],
            'userAddedMeal' => $userAddedMeal,
            'createdBy'     => $createdBy,
            'ratings'       => Rating::get(),
            'userRating'    => $userRating,
            'globalRating'  => $globalRating
        ]);
    }

    public function edit($id)
    {
        $createdBy = Meal::where('id', $id)->first()->created_by;

        if ($createdBy == \Auth::user()->id) {
            $meal = Meal::join('images', 'images.meal_id', '=', 'meals.id')
                ->where('meals.id', $id)
                ->get([
                    'meals.id',
                    'meals.title',
                    'meals.description',
                    'meals.ingredients',
                    'meals.method',
                    'meals.serving',
                    'meals.cal',
                    'meals.vegan',
                    'meals.time',
                    'meals.slow_cook',
                    'meals.budget',
                    'meals.url',
                    'meals.rating',
                    'meals.public',
                    'images.name as image_name'
                ]);

            return view('pages.meals.edit', [
                'meal'    => $meal[0],
                'ratings' => Rating::get()
            ]);

        } else {
            return redirect('/meals')->with('error', 'Only the meal creator can edit this meal');
        }
    }

    public function update(Request $request, $id)
    {
        $meal = Meal::find($id);

        $mealTitle = $request->title;

        $meal->update([
            'title'       => $mealTitle,
            'description' => $request->description,
            'ingredients' => $request->ingredients,
            'method'      => $request->meal_method,
            'serving'     => $request->serving,
            'cal'         => $request->cal,
            'vegan'       => $request->vegan,
            'time'        => $request->time,
            'slow_cook'   => $request->slow_cook,
            'budget'      => $request->budget,
            'url'         => $request->url,
            'rating'      => $request->rating,
            'public'      => $request->public == 1 ? 1 : 0,
            'created_at'  => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'  => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        if ($request->file('image')) {
            $image           = $request->file('image');
            $filename        = $image->getClientOriginalName();
            $destinationPath = 'images/' . $mealTitle;
            $image->storeAs("$destinationPath", $filename);

            $previousImage = Image::where('meal_id', $meal->id)->first();

            $previousDestinationPath = 'images/' . $mealTitle . '/' . $previousImage->name;
            File::delete($previousDestinationPath);

            $previousImage->delete();

            Image::create([
                'meal_id'    => $meal->id,
                'path'       => $destinationPath . '/' . $filename,
                'name'       => $filename,
                'created_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);
        }

        return redirect()->back()->with('success', 'Updated meal successfully!');
    }

    public function userRating(Request $request, $mealId)
    {
        $meal = UserMeal::where('user_id', \Auth::user()->id)->where('meal_id', $mealId)->first();

        $meal->update(['rating' => $request->rating]);

        return redirect()->back();
    }

    public function deleteMeal($mealId)
    {
        $createdBy    = Meal::where('id', $mealId)->first()->created_by;
        $mealToDelete = Meal::find($mealId);

        if ($createdBy == \Auth::user()->id) {

            $mealOnPlans = MealPlanData::where('meal_id', $mealId)->get();

            foreach ($mealOnPlans as $meal) {
                $meal->update(['meal_id' => NULL]);
            }

            $mealToDelete->delete();

            return redirect('/meals')->with('success', 'Meal deleted');
        } else {
            return redirect('/meals')->with('error', 'Only the meal creator can delete this meal');
        }
    }

    public function addMealToUser($mealId)
    {
        UserMeal::create([
            'user_id' => \Auth::user()->id,
            'meal_id' => $mealId
        ]);

        return redirect('/meals');
    }

    public function removeMealFromUser($mealId)
    {
        UserMeal::where('user_id', \Auth::user()->id)->where('meal_id', $mealId)->first()->delete();

        return redirect('/meals')->with('success', 'Removed meal from your list');
    }
}
