<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MealPlanData extends Model
{
    use HasFactory;

    protected $table = 'meal_plan_data';
    protected $guarded = [];
    public $timestamps = false;
}
